import MonstieType from "./MonstieType";

export default interface Monstie{
    id: number;
    name: string;
    locations: string[];
    type: MonstieType
}