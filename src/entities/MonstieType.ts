enum MonstieType{
    Amphibian = 'Amphibian',
    BirdWyvern = 'BirdWyvern',
    BruteWyvern = 'BruteWyvern',
    ElderDragon = 'ElderDragon',
    FangedBeast = 'FangedBeast',
    FangedWyvern = 'FangedWyvern',
    FlyingWyvern = 'FlyingWyvern',
    Herbivore = 'Herbivore',
    Leviathan = 'Leviathan',
    PiscineWyvern = 'PiscineWyvern',
    Temnoceran = 'Temnoceran'
}

export default MonstieType

export interface Type{
    type: MonstieType;
    pattern: string;
}
