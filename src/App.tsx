import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import './App.css';
import MonstiesList from './components/MonstiesList';
import MonstieTypeList from './components/MonstieTypeList';

function App() {
  return (
    <Router>
      <div className="App" style= {{ backgroundImage: "url(/images/backgrounds/1.webp)"}}>
        <header className="App-header" >
          <h1>Monster Hunter Stories 2: Wings of Ruin</h1>
        </header>

        <Switch>
          <Route path="/:monstieType">
            <MonstiesList/>
          </Route>
          <Route path="/">
            <MonstieTypeList/>
          </Route>
        </Switch>
        <div id="copyright">
          Developed by <a href="https://twitter.com/ClaraDiazRuiz1">Clara</a> y <a href="https://twitter.com/2balric">Alex</a> 💓
        </div>
      </div>
    </Router>
  );
}

export default App;
