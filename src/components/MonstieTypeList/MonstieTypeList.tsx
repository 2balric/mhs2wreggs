import React from 'react'
import { Link } from "react-router-dom";
import MonstieType from '../../entities/MonstieType';

import './MonstieTypeList.css'

const MonstieTypeList = () => {
    return (
        <div className="wrapper">
        {Object.keys(MonstieType).map( type => 
          <article key={type}>
            <Link to={`/${type}`}>
              <img src={`/images/types/${type}.webp`} alt={type}/>  
            </Link>
          </article>
        )}
      </div>
    )
}

export default MonstieTypeList