import React from 'react'
import MonstieTypeList from '.'

const story = {
    title: "MonstieTypeList",
    component: MonstieTypeList
}
export default story

export const MonstieTypeListExample = () => <MonstieTypeList/>