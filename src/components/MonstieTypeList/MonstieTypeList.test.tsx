import React from 'react'
import { render } from '@testing-library/react'
import MonstieTypeList from '.'

test("MonstieTypeList renders", async () => {
    const { findByText } = render(<MonstieTypeList/>)
    
    const textComponent = await findByText(/MonstieTypeList/)
    
    expect(textComponent).toHaveTextContent("MonstieTypeList");
})