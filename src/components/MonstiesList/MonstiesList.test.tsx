import React from 'react'
import { render } from '@testing-library/react'
import MonstiesList from '.'

test("MonstiesList renders", async () => {
    const { findByText } = render(<MonstiesList/>)
    
    const textComponent = await findByText(/MonstiesList/)
    
    expect(textComponent).toHaveTextContent("MonstiesList");
})