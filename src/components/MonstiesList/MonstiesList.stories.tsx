import React from 'react'
import MonstiesList from '.'

const story = {
    title: "MonstiesList",
    component: MonstiesList
}
export default story

export const MonstiesListExample = () => <MonstiesList/>