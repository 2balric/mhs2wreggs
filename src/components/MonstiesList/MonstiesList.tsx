import React, {useState, useEffect} from 'react'
import { useParams, Link } from 'react-router-dom'
import Monstie from '../../entities/Monstie'
import MonstieItem from '../MonstieItem'
import monstipedia from './../../db/Monstipedia'

import './MonstiesList.css'

type Prop = {
    
}

interface RouteParams {
    monstieType: string
}

const MonstiesList : React.FC<Prop> = () => {
    const [monsties, setMonsties] = useState<Monstie[]>([])
    const { monstieType } = useParams<RouteParams>()
    useEffect(() => {
        setMonsties( old => [...old, ...monstipedia.filter( m => m.type === monstieType)])
    }, [monstieType])
    return (
        <div>
            <div className="title">
                <Link to="/">{"< "}Atrás</Link>
                <h1>{monstieType} 🐉</h1>
            </div>            
            <div className="monstie-list">
            {
                monsties.map( monstei => <MonstieItem key={monstei.id} monstie={monstei}/>)
            }
            </div>
        </div>
    )
}

export default MonstiesList