import React from 'react'
import { render } from '@testing-library/react'
import MonstieItem from '.'
import Monstie from '../../entities/Monstie'
import MonstieType from '../../entities/MonstieType'

const monstie : Monstie =
{
    id: 1,
    name: "Aptonoth",
    type: MonstieType.Herbivore,
    locations: ["Hakolo"]
}

test("Monstie renders", async () => {
    const { findByText } = render(<MonstieItem monstie={monstie}/>)
    
    const textComponent = await findByText(/Monstie/)
    
    expect(textComponent).toHaveTextContent("Monstie");
})