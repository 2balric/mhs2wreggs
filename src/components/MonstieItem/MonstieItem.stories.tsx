import React from 'react'
import MonstieItem from '.'

import Monstie from '../../entities/Monstie'
import MonstieType from '../../entities/MonstieType'

const story = {
    title: "Monstie",
    component: MonstieItem
}
export default story

const monstie : Monstie =
{
    id: 1,
    name: "Aptonoth",
    type: MonstieType.Herbivore,
    locations: ["Hakolo"]
}
export const MonstieExample = () => <MonstieItem monstie={monstie}/>