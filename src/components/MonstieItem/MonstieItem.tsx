import React from 'react'
import Monstie from '../../entities/Monstie'

import './MonstieItem.css'

type Prop = {
    monstie: Monstie
}

const MonstieItem : React.FC<Prop> = ({monstie}) => {
    return (
        <div className="monstei-card">
            <div className="monstei-card__egg">
                <img loading="lazy" src={`/images/eggs/${monstie.id}.webp`} alt={monstie.name}/>
                <h2>{ monstie.name }</h2>
            </div>
            <div className="monstei-card__image">
                <img loading="lazy" src={`/images/monsties/${monstie.id}.jpeg`} alt={monstie.name}/>
            </div>
        </div>
    )
}

export default MonstieItem