import Monstie from "../entities/Monstie";
import MonstieType from "../entities/MonstieType";

const monstipedia : Monstie[] = [
    {
        id: 1,
        name: "Aptonoth",
        type: MonstieType.Herbivore,
        locations: ["Hakolo"]
    },
    {
        id: 5,
        name: "Velocidrome",
        type: MonstieType.BirdWyvern,
        locations: ["Hakolo"]
    },
    {
        id: 6,
        name: "Kulu-Ya-Ku",
        type: MonstieType.BirdWyvern,
        locations: ["Hakolo"]
    },
    {
        id: 9,
        name: "Yian Kut-Ku",
        type: MonstieType.BirdWyvern,
        locations: ["Hakolo"]
    },
    {
        id: 11,
        name: "Bulldrome",
        type: MonstieType.FangedBeast,
        locations: ["Hakolo"]
    },
    {
        id: 12,
        name: "Pukei-Pukei",
        type: MonstieType.BirdWyvern,
        locations: ["Hakolo"]
    },
    {
        id: 13,
        name: "Apceros",
        type: MonstieType.BirdWyvern,
        locations: ["Alcala"]
    },
    {
        id: 18,
        name: "Gran Jaggi",
        type: MonstieType.BirdWyvern,
        locations: ["Alcala"]
    },
    {
        id: 20,
        name: "Ludroth Real",
        type: MonstieType.Leviathan,
        locations: ["Alcala"]
    },
    {
        id: 22,
        name: "Arzuros",
        type: MonstieType.FangedBeast,
        locations: ["Alcala"]
    },
    {
        id: 23,
        name: "Qurupeco",
        type: MonstieType.BirdWyvern,
        locations: ["Alcala"]
    },
    {
        id: 24,
        name: "Yian Kut-Ku Azul",
        type: MonstieType.BirdWyvern,
        locations: ["Alcala"]
    },
    {
        id: 25,
        name: "Tigrex",
        type: MonstieType.FlyingWyvern,
        locations: ["Everywhere"]
    },
    {
        id: 26,
        name: "Monoblos",
        type: MonstieType.FlyingWyvern,
        locations: ["Alcala", "Lamure"]
    },
    {
        id: 27,
        name: "Paolumu",
        type: MonstieType.FlyingWyvern,
        locations: ["Alcala"]
    },
    {
        id: 28,
        name: "Basarios",
        type: MonstieType.FlyingWyvern,
        locations: ["Terga"]
    },
    {
        id: 29,
        name: "Yian Garuga",
        type: MonstieType.BirdWyvern,
        locations: ["Alcala"]
    },
    {
        id: 31,
        name: "Nargacuga",
        type: MonstieType.FlyingWyvern,
        locations: ["Alcala"]
    },
    {
        id: 32,
        name: "Rathian",
        type: MonstieType.FlyingWyvern,
        locations: ["Alcala"]
    },
    {
        id: 33,
        name: "Popo",
        type: MonstieType.Herbivore,
        locations: ["Loloska"]
    },
    {
        id: 37,
        name: "Congalala",
        type: MonstieType.FangedBeast,
        locations: ["Loloska"]
    },
    {
        id: 39,
        name: "Gran Baggi",
        type: MonstieType.BirdWyvern,
        locations: ["Loloska"]
    },
    {
        id: 40,
        name: "Kecha Wacha",
        type: MonstieType.FangedBeast,
        locations: ["Loloska"]
    },
    {
        id: 41,
        name: "Gypceros",
        type: MonstieType.BirdWyvern,
        locations: []
    },
    {
        id: 43,
        name: "Zamtrios",
        type: MonstieType.Amphibian,
        locations: ["Loloska"]
    },
    {
        id: 44,
        name: "Nerscylla",
        type: MonstieType.Temnoceran,
        locations: ["Loloska"]
    },
    {
        id: 45,
        name: "Barroth",
        type: MonstieType.BruteWyvern,
        locations: ["Loloska"]
    },
    {
        id: 46,
        name: "Tobi-Kadachi",
        type: MonstieType.FangedWyvern,
        locations: ["Loloska"]
    },
    {
        id: 47,
        name: "Khezu",
        type: MonstieType.FlyingWyvern,
        locations: ["Loloska"]
    },
    {
        id: 48,
        name: "Khezu Rojo",
        type: MonstieType.FlyingWyvern,
        locations: ["Loloska"]
    },
    {
        id: 51,
        name: "Lagombi",
        type: MonstieType.FangedBeast,
        locations: ["Loloska"]
    },
    {
        id: 52,
        name: "Barroth Esmeralda",
        type: MonstieType.BruteWyvern,
        locations: ["Loloska"]
    },
    {
        id: 53,
        name: "Anjanath",
        type: MonstieType.BruteWyvern,
        locations: ["Loloska"]
    },
    {
        id: 54,
        name: "Gammoth",
        type: MonstieType.FangedBeast,
        locations: ["Loloska"]
    },
    {
        id: 55,
        name: "Zinogre",
        type: MonstieType.FangedWyvern,
        locations: ["Loloska", "Terga"]
    },
    {
        id: 56,
        name: "Barioth",
        type: MonstieType.FlyingWyvern,
        locations: ["Loloska"]
    },
    {
        id: 57,
        name: "Legiana",
        type: MonstieType.FlyingWyvern,
        locations: ["Loloska"]
    },
    {
        id: 65,
        name: "Iodrome",
        type: MonstieType.BirdWyvern,
        locations: ["Terga"]
    },
    {
        id: 67,
        name: "Gendrome",
        type: MonstieType.BirdWyvern,
        locations: ["Lamure Desert"]
    },
    {
        id: 69,
        name: "Cephadrome",
        type: MonstieType.PiscineWyvern,
        locations: ["Lamure Desert"]
    },
    {
        id: 70,
        name: "Kecha Wacha Ceniza",
        type: MonstieType.FangedBeast,
        locations: ["Lamure Tower", "Hakolo"]
    },
    {
        id: 71,
        name: "Gypceros Morado",
        type: MonstieType.BirdWyvern,
        locations: []
    },
    {
        id: 72,
        name: "Nargacuga Verde",
        type: MonstieType.FlyingWyvern,
        locations: ["Pomore Garden", "Hakolo"]
    },
    {
        id: 73,
        name: "Diablos",
        type: MonstieType.FlyingWyvern,
        locations: ["Lamure"]
    },
    {
        id: 74,
        name: "Diablos Negro",
        type: MonstieType.FlyingWyvern,
        locations: ["Lamure"]
    },
    {
        id: 75,
        name: "Monoblos Blanco",
        type: MonstieType.FlyingWyvern,
        locations: ["Lamure", "Kakolo"]
    },
    {
        id: 76,
        name: "Qurupeco Carmesí",
        type: MonstieType.BirdWyvern,
        locations: ["Pomore Garden"]
    },
    {
        id: 77,
        name: "Mizutsune",
        type: MonstieType.Leviathan,
        locations: ["Pomore Garden"]
    },
    {
        id: 78,
        name: "Lagiacrus",
        type: MonstieType.Leviathan,
        locations: ["Pomore Garden"]
    },
    {
        id: 79,
        name: "Uragaan",
        type: MonstieType.BruteWyvern,
        locations: ["Terga"]
    },
    {
        id: 82,
        name: "Tigrex Pardo",
        type: MonstieType.FlyingWyvern,
        locations: ["Everywhere"]
    },
    {
        id: 83,
        name: "Ludroth Púrpura",
        type: MonstieType.Leviathan,
        locations: ["Lamure"]
    },
    {
        id: 84,
        name: "Basarios Rubí",
        type: MonstieType.FlyingWyvern,
        locations: ["Pomore Garden"]
    },
    {
        id: 85,
        name: "Gravios",
        type: MonstieType.FlyingWyvern,
        locations: ["Terga"]
    },
    {
        id: 92,
        name: "Nerscylla Shrouded",
        type: MonstieType.Temnoceran,
        locations: ["Terga"]
    },
    {
        id: 93,
        name: "Congalala esmeralda",
        type: MonstieType.FangedBeast,
        locations: ["Terga"]
    },
    {
        id: 95,
        name: "Gravios negro",
        type: MonstieType.FlyingWyvern,
        locations: ["Terga"]
    },
    {
        id: 96,
        name: "Lagiacrus Ivory",
        type: MonstieType.Leviathan,
        locations: ["Terga"]
    },
    {
        id: 97,
        name: "Brachydios",
        type: MonstieType.BruteWyvern,
        locations: ["Terga"]
    },
    {
        id: 98,
        name: "Nergigante",
        type: MonstieType.ElderDragon,
        locations: ["Terga"]
    },
    {
        id: 99,
        name: "Astalos",
        type: MonstieType.FlyingWyvern,
        locations: ["Terga"]
    },
    {
        id: 100,
        name: "Glavenus",
        type: MonstieType.BruteWyvern,
        locations: ["Terga", "loloska"]
    },
    {
        id: 102,
        name: "Bazelgeuse",
        type: MonstieType.FlyingWyvern,
        locations: ["Everywhere"]
    },
    {
        id: 107,
        name: "Barioth Arena",
        type: MonstieType.FlyingWyvern,
        locations: ["Hakolo"]
    },
    {
        id: 108,
        name: "Deviljho",
        type: MonstieType.BruteWyvern,
        locations: ["Everywhere"]
    },
    {
        id: 109,
        name: "Zinogre Stygian",
        type: MonstieType.FangedWyvern,
        locations: ["Hakolo"]
    },
    {
        id: 110,
        name: "Seregios",
        type: MonstieType.FlyingWyvern,
        locations: ["Hakolo"]
    },
    {
        id: 112,
        name: "Rathalos",
        type: MonstieType.FlyingWyvern,
        locations: ["Hakolo"]
    },
    {
        id: 113,
        name: "Rathalos Celeste",
        type: MonstieType.FlyingWyvern,
        locations: ["Hakolo"]
    },
    {
        id: 114,
        name: "Rathian Rosa",
        type: MonstieType.FlyingWyvern,
        locations: ["Pomore garden"]
    },
    {
        id: 115,
        name: "Anjanath Fulgur",
        type: MonstieType.BruteWyvern,
        locations: ["Loloska"]
    },
    {
        id: 116,
        name: "Nargacuga Argenteo",
        type: MonstieType.FlyingWyvern,
        locations: ["Hakolo"]
    },
    {
        id: 118,
        name: "Diablos Sanguinario",
        type: MonstieType.FlyingWyvern,
        locations: ["Lamure"]
    },
    {
        id: 117,
        name: "Zinogre Tormenta",
        type: MonstieType.FangedWyvern,
        locations: ["Loloska"]
    },
    {
        id: 120,
        name: "Rathian Maldita",
        type: MonstieType.FlyingWyvern,
        locations: ["Terga"]
    },
    {
        id: 119,
        name: "Tigrex Guadaña",
        type: MonstieType.FlyingWyvern,
        locations: ["Alcala"]
    },
    {
        id: 124,
        name: "Kushala Daora",
        type: MonstieType.ElderDragon,
        locations: ["Alcala"]
    },
    {
        id: 121,
        name: "Kirin",
        type: MonstieType.ElderDragon,
        locations: ["Lamure"]
    },
    {
        id: 122,
        name: "Teostra",
        type: MonstieType.ElderDragon,
        locations: ["Terga"]
    },
    {
        id: 125,
        name: "Velkhana",
        type: MonstieType.ElderDragon,
        locations: ["Loloska"]
    },
]
export default monstipedia